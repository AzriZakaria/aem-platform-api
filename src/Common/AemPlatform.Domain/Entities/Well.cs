﻿using AemPlatform.Domain.Common;

namespace AemPlatform.Domain.Entities
{
    public class Well : BaseEntity
    {
        public int Id { get; set; }
        public int PlatformId { get; set; }
        public Platform Platform { get; set; }
        public string UniqueName { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

    }
}
