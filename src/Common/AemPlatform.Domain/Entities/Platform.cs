﻿using AemPlatform.Domain.Common;
using System.Collections.Generic;

namespace AemPlatform.Domain.Entities
{
    public class Platform : BaseEntity
    {
        public int Id { get; set; }
        public string UniqueName { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public List<Well> Wells { get; set; }
    }






}
