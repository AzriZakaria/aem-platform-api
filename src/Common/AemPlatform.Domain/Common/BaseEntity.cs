﻿using System;

namespace AemPlatform.Domain.Common
{
    public abstract class BaseEntity
    {
        private DateTime createdAt = DateTime.Now.ToUniversalTime();
        public DateTime CreatedAt { get => createdAt; set => createdAt = value; }

        private DateTime updatedAt = DateTime.Now.ToUniversalTime();
        public DateTime UpdatedAt { get => updatedAt; set => updatedAt = value; }

        private string createdBy { get; set; }
        public string CreatedBy { get => createdBy; set => createdBy = value; }
        private string updateBy { get; set; }
        public string UpdateBy { get => updateBy; set => updateBy = value; }


    }
}
