﻿using System;
using System.Collections.Generic;

namespace AemPlatform.Application.ExternalServices.Aemenersol.Response
{
    public class Well
    {
        public int Id { get; set; }
        public int PlatformId { get; set; }
        public string UniqueName { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime LastUpdate { get; set; }
    }

    public class PlatformWellDummyResponse
    {
        public int Id { get; set; }
        public string UniqueName { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime LastUpdate { get; set; }
        public List<Well> Well { get; set; }
    }


}
