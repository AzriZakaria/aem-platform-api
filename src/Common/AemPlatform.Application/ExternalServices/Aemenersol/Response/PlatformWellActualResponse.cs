﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AemPlatform.Application.ExternalServices.Aemenersol.Response
{

    public class WellActual
    {
        public int Id { get; set; }
        public int PlatformId { get; set; }
        public string UniqueName { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    public class PlatformWellActualResponse
    {
        public int Id { get; set; }
        public string UniqueName { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public List<WellActual> Well { get; set; }
    }
}
