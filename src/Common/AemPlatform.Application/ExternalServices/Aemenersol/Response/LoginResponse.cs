﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AemPlatform.Application.ExternalServices.Aemenersol.Response
{
    public class LoginResponse
    {
        public string JwtToken { get; set; }
    }
}
