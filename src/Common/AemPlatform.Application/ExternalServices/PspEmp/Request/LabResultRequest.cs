﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AemPlatform.Application.ExternalServices.PspEmp.Request
{
    public class LabResultRequest
    {
        public string IdNo { get; set; }
        public string Token { get; set; }
    }
}
