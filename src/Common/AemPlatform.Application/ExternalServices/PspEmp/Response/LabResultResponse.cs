﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AemPlatform.Application.ExternalServices.PspEmp.Response
{
    public class LabResultResponse
    {
        public LabResultResponse()
        {
            EntryId = Guid.NewGuid().ToString("N");
        }
        public string EntryId { get; set; }

        public string FhirVersion { get; set; }
        public FhirBundle FhirBundle { get; set; }
    }


    public class Code
    {
        public string Text { get; set; }
        public List<Coding> Coding { get; set; }
    }

    public class Extension
    {
        public string Url { get; set; }
        public Code Code { get; set; }
    }

    public class Identifier
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }

    public class Collection
    {
        public DateTime CollectedDateTime { get; set; }
    }

    public class Coding
    {
        public string System { get; set; }
        public string Code { get; set; }
        public string Display { get; set; }
    }

    public class ValueCodeableConcept
    {
        public List<Coding> Coding { get; set; }
    }

    public class Name
    {
        public string Text { get; set; }
    }

    public class Performer
    {
        public List<Name> Name { get; set; }
    }

    public class Qualification
    {
        public string Identifier { get; set; }
        public string Issuer { get; set; }
    }

    public class Endpoint
    {
        public string Address { get; set; }
    }

    public class Telecom
    {
        public string System { get; set; }
        public string Value { get; set; }
    }

    public class Address
    {
        public string Type { get; set; }
        public string Use { get; set; }
        public string Text { get; set; }
    }

    public class Contact
    {
        public List<Telecom> Telecom { get; set; }
        public Address Address { get; set; }
    }

    public class Entry
    {

        public string ResourceType { get; set; }
        public List<Extension> Extension { get; set; }
        public List<Identifier> Identifier { get; set; }
        public object Name { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public object Type { get; set; }
        public Collection Collection { get; set; }
        public string Status { get; set; }
        public Code Code { get; set; }
        public ValueCodeableConcept ValueCodeableConcept { get; set; }
        public DateTime? EffectiveDateTime { get; set; }
        public Performer Performer { get; set; }
        public List<Qualification> Qualification { get; set; }
        public Endpoint Endpoint { get; set; }
        public Contact Contact { get; set; }
    }

    public class FhirBundle
    {
        public string ResourceType { get; set; }
        public string Type { get; set; }
        public List<Entry> Entry { get; set; }
    }
}
