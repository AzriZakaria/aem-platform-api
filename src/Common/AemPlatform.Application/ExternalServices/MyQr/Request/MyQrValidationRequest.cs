﻿using System.Text.Json.Serialization;
using Mapster;

namespace AemPlatform.Application.ExternalServices.MyQr.Request
{
    public class MyQrValidationRequest 
    {
       
        public string QrContent { get; set; }
        [JsonPropertyName("temporary_id")]
        public string TemporaryId
        {
            get { return QrContent; }
        }
    }
}
