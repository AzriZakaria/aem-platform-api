﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace AemPlatform.Application.ExternalServices.MyQr.Response
{
    public class MyQrValidationResponse
    {
        [JsonPropertyName("status_code")]
        public string StatusCode { get; set; }
        public Data Data { get; set; }
        public PersonalDetails Patient { get; set; }
        public Info Info { get; set; }
        public List<CovidTest> CovidTest { get; set; }
    }

    public class Code
    {
        public string Text { get; set; }
        [JsonPropertyName("coding")]
        public List<Coding> Coding { get; set; }
    }

    public class Extension
    {
        public string Url { get; set; }
        public Code Code { get; set; }
    }

    public class Identifier
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }

    public class Collection
    {
        public DateTime CollectedDateTime { get; set; }
    }

    public class Coding
    {
        [JsonPropertyName("system")]
        public string System { get; set; }
        [JsonPropertyName("code")]
        public string Code { get; set; }
        [JsonPropertyName("display")]
        public string Display { get; set; }
    }

    public class ValueCodeableConcept
    {
        [JsonPropertyName("coding")]
        public List<Coding> Coding { get; set; }
    }

    public class Name
    {
        public string Text { get; set; }
    }
    public class Type
    {
        public string Text { get; set; }
    }

    public class Performer
    {
        public List<Name> Name { get; set; }
    }

    public class Qualification
    {
        public string Identifier { get; set; }
        public string Issuer { get; set; }
    }

    public class Endpoint
    {
        public string Address { get; set; }
    }

    public class Telecom
    {
        public string System { get; set; }
        public string Value { get; set; }
    }

    public class Address
    {
        public string Type { get; set; }
        public string Use { get; set; }
        public string Text { get; set; }
    }

    public class Contact
    {
        public List<Telecom> Telecom { get; set; }
        public Address Address { get; set; }
    }

    public class Entry
    {
        public string ResourceType { get; set; }
        public List<Extension> Extension { get; set; }
        public List<Identifier> Identifier { get; set; }
        public object Name { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public object Type { get; set; }
        public Collection Collection { get; set; }
        public string Status { get; set; }
        public Code Code { get; set; }
        public ValueCodeableConcept ValueCodeableConcept { get; set; }
        public DateTime? EffectiveDateTime { get; set; }
        public Performer Performer { get; set; }
        public List<Qualification> Qualification { get; set; }
        public Endpoint Endpoint { get; set; }
        public Contact Contact { get; set; }
    }



    public class FhirBundle
    {
        public string ResourceType { get; set; }
        public string Type { get; set; }
        public List<Entry> Entry { get; set; }
    }

    public class Data
    {
        public string FhirVersion { get; set; }
        public FhirBundle FhirBundle { get; set; }
       
    }

    public class PersonalDetails
    {
        public string Passport { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Nationality { get; set; }
        public string Gender { get; set; }
        public string NationalId { get; set; }

    }
    public class Info
    {
        public List<string> Type { get; set; }
        public string DocStandard { get; set; }
        public IssuerNetwork IssuerNetwork { get; set; }
        public bool IsVaccineDosageComplete { get; set; }
    }

    public class IssuerNetwork
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string Id { get; set; }
    }
    public class Provider
    {
        public Lab Lab { get; set; }
        public HealthcareInstitution HealthcareInstitution { get; set; }
    }

    public class HealthcareInstitution
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Address1
        {
            get { return Address; }
            set { Address = value; }
        }
        public string Country { get; set; }
        public string Id { get; set; }
    }
    public class Lab
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string Id { get; set; }
    }

    public class CovidTest
    {
        public string TestResult { get; set; }
        public string TestResultSimplified { get; set; }
        public string TestType { get; set; }
        public string TestTypeSimplified { get; set; }
        public string CollectionMethod { get; set; }
        public string TestResultSnomedCode { get; set; }
        public string TestTypeLoincCode { get; set; }
        public string CollectionMethodSnomedCode { get; set; }
        public DateTime CollectionDate { get; set; }
        public DateTime TestReportDate { get; set; }
        public Provider Provider { get; set; }
    }

}
