﻿using AemPlatform.Domain.Entities;
using Mapster;
using System;
using System.Collections.Generic;

namespace AemPlatform.Application.Dto
{
    public class PlatformDto : IRegister
    {
        public PlatformDto()
        {
            Wells = new List<WellDto>();
        }


        public int Id { get; set; }
        public string UniqueName { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public void Register(TypeAdapterConfig config)
        {
            
            config.NewConfig<Domain.Entities.Platform, PlatformDto>();
        }

        public IList<WellDto> Wells { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }





    }


}

