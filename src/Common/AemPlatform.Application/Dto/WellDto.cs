﻿using AemPlatform.Domain.Entities;
using Mapster;
using System;

namespace AemPlatform.Application.Dto
{
    public class WellDto : IRegister
    {      
        public int Id { get; set; }
        public string UniqueName { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Well, WellDto>();
           
        }
    }
}
