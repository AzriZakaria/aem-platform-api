﻿using AemPlatform.Application.Common.Interfaces;
using AemPlatform.Application.Common.Models;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AemPlatform.Application.Platform.Commands.Create
{
    public class CreatePlatformDataFromApiCommand : IRequestWrapper<bool>
    {
        public bool UseActualData { get; set; }
    }

    public class CreatePlatformDataFromApiCommandHandler : IRequestHandlerWrapper<CreatePlatformDataFromApiCommand, bool>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IAemenersolServices _aemenersolServices;
        private readonly IMemoryCache _cache;
        private readonly IConfiguration _configuration;



        public CreatePlatformDataFromApiCommandHandler(IApplicationDbContext context, IConfiguration configuration, IMapper mapper, IAemenersolServices aemenersolServices, IMemoryCache memoryCache)
        {
            _context = context;
            _mapper = mapper;
            _aemenersolServices = aemenersolServices;
            _cache = memoryCache;
            _configuration = configuration;
        }

        public async Task<ServiceResult<bool>> Handle(CreatePlatformDataFromApiCommand request, CancellationToken cancellationToken)
        {
            string token = string.Empty;

            if (!_cache.TryGetValue(CacheKeys.TokenSource, out token))
            {
                string username = _configuration.GetSection("AemenersolApi:Username").Value;
                string password = _configuration.GetSection("AemenersolApi:Password").Value;

                var jwtToken = await _aemenersolServices.GetToken(new ExternalServices.Aemenersol.Request.LoginRequest {Username = username,Password = password }, cancellationToken);
                
                var handler = new JwtSecurityTokenHandler();
                var jwtSecurityToken = handler.ReadJwtToken(jwtToken.Data);

                token = jwtToken.Data;

                if (jwtSecurityToken.ValidTo != DateTime.MinValue)
                {
                    TimeSpan timeExpired = jwtSecurityToken.ValidTo - DateTime.Now;
                    // Set cache options.
                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                        .SetSlidingExpiration(timeExpired);

                    _cache.Set(CacheKeys.TokenSource, jwtToken.Data, cacheEntryOptions);
                }
                else
                {
                    _cache.Set(CacheKeys.TokenSource, jwtToken.Data);
                }

            }


            if (request.UseActualData)
            {
                var getActualData = await _aemenersolServices.GetPlatformDataActual(string.Format("Bearer {0}",token), cancellationToken);


                if(getActualData.Data == null)
                {
                    return ServiceResult.Success(false);
                }

                if(getActualData.Data.Any())
                {
                    foreach(var eachPlatform in getActualData.Data)
                    {
                        var getPlatform = await _context.Platform.FirstOrDefaultAsync(c => c.Id == eachPlatform.Id);

                        if (getPlatform != null)
                        {
                            getPlatform.UniqueName = eachPlatform.UniqueName;
                            getPlatform.CreatedAt = eachPlatform.CreatedAt;
                            getPlatform.UpdatedAt = eachPlatform.UpdatedAt;
                            getPlatform.Longitude = eachPlatform.Longitude;
                            getPlatform.Latitude = eachPlatform.Latitude;

                            await _context.SaveChangesAsync(cancellationToken);

                            if (eachPlatform.Well.Any())
                            {
                                var addWells = new List<Domain.Entities.Well>();

                                foreach (var eachWell in eachPlatform.Well)
                                {
                                    var getWell = await _context.Well.FirstOrDefaultAsync(c => c.Id == eachWell.Id);

                                    if (getWell != null)
                                    {
                                        getWell.UniqueName = eachWell.UniqueName;
                                        getWell.CreatedAt = eachWell.CreatedAt;
                                        getWell.UpdatedAt = eachWell.UpdatedAt;
                                        getWell.Longitude = eachWell.Longitude;
                                        getWell.Latitude = eachWell.Latitude;
                                    }

                                    await _context.SaveChangesAsync(cancellationToken);
                                }
                            }
                        }
                        else
                        {
                            var newPlatform = new Domain.Entities.Platform
                            {
                                Id = eachPlatform.Id,
                                UniqueName = eachPlatform.UniqueName,
                                CreatedAt = eachPlatform.CreatedAt,
                                UpdatedAt = eachPlatform.UpdatedAt,
                                Longitude = eachPlatform.Longitude,
                                Latitude = eachPlatform.Latitude,
                            };

                            await _context.Platform.AddAsync(newPlatform, cancellationToken);

                            await _context.SaveChangesAsync(cancellationToken);

                            if(eachPlatform.Well.Any())
                            {

                                foreach (var eachWell in eachPlatform.Well)
                                {
                                    var newWell = new Domain.Entities.Well
                                    {
                                        Id = eachWell.Id,
                                        UniqueName = eachWell.UniqueName,
                                        CreatedAt = eachWell.CreatedAt,
                                        UpdatedAt = eachWell.UpdatedAt,
                                        Longitude = eachWell.Longitude,
                                        Latitude = eachWell.Latitude,
                                        PlatformId = eachWell.PlatformId,
                                    };

                                    await _context.Well.AddAsync(newWell);

                                    await _context.SaveChangesAsync(cancellationToken);

                                }



                            }
                            
                        }
                    }
                }
            }
            else
            {
                var getDummyData = await _aemenersolServices.GetPlatformDummy(string.Format("Bearer {0}", token), cancellationToken);

                if (getDummyData.Data == null)
                {
                    return ServiceResult.Success(false);
                }
                if (getDummyData.Data.Any())
                {
                    foreach (var eachPlatform in getDummyData.Data)
                    {
                        var getPlatform = await _context.Platform.FirstOrDefaultAsync(c => c.Id == eachPlatform.Id);

                        if (getPlatform != null)
                        {
                            getPlatform.UniqueName = eachPlatform.UniqueName;
                            getPlatform.UpdatedAt = eachPlatform.LastUpdate;
                            getPlatform.Longitude = eachPlatform.Longitude;
                            getPlatform.Latitude = eachPlatform.Latitude;

                            await _context.SaveChangesAsync(cancellationToken);

                            if (eachPlatform.Well.Any())
                            {
                                foreach (var eachWell in eachPlatform.Well)
                                {
                                    var getWell = await _context.Well.FirstOrDefaultAsync(c => c.Id == eachWell.Id);

                                    if (getWell != null)
                                    {
                                        getWell.UniqueName = eachWell.UniqueName;
                                        getWell.UpdatedAt = eachWell.LastUpdate;
                                        getWell.Longitude = eachWell.Longitude;
                                        getWell.Latitude = eachWell.Latitude;
                                    }

                                    await _context.SaveChangesAsync(cancellationToken);
                                }
                            }
                        }
                      
                    }
                }
            }

            return ServiceResult.Success(true);

        }
    }
}
