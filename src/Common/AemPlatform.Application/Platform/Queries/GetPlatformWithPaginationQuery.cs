﻿using AemPlatform.Application.Common.Interfaces;
using AemPlatform.Application.Common.Mapping;
using AemPlatform.Application.Common.Models;
using AemPlatform.Application.Dto;
using Mapster;
using MapsterMapper;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AemPlatform.Application.Platform.Queries
{

    public class GetPlatformWithPaginationQuery : IRequestWrapper<PaginatedList<PlatformDto>>
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }

    public class GetPlatformWithPaginationQueryHandler : IRequestHandlerWrapper<GetPlatformWithPaginationQuery, PaginatedList<PlatformDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetPlatformWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<PaginatedList<PlatformDto>>> Handle(GetPlatformWithPaginationQuery request, CancellationToken cancellationToken)
        {
            PaginatedList<PlatformDto> list = await _context.Platform
                .OrderBy(o => o.Id)
                .ProjectToType<PlatformDto>(_mapper.Config)
                .PaginatedListAsync(request.PageNumber, request.PageSize);

            return list.Items.Any() ? ServiceResult.Success(list) : ServiceResult.Failed<PaginatedList<PlatformDto>>(ServiceError.NotFound);
        }
    }
}
