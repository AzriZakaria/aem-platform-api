﻿using AemPlatform.Application.Common.Interfaces;
using AemPlatform.Application.Common.Mapping;
using AemPlatform.Application.Common.Models;
using AemPlatform.Application.Dto;
using Mapster;
using MapsterMapper;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AemPlatform.Application.Platform.Queries
{

    public class GetWellsWithPaginationQuery : IRequestWrapper<PaginatedList<WellDto>>
    {
        public int PlaftformId { get; set; }
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }

    public class GetWellsWithPaginationQueryHandler : IRequestHandlerWrapper<GetWellsWithPaginationQuery, PaginatedList<WellDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetWellsWithPaginationQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ServiceResult<PaginatedList<WellDto>>> Handle(GetWellsWithPaginationQuery request, CancellationToken cancellationToken)
        {
            PaginatedList<WellDto> list = await _context.Well
                .Where(c=>c.PlatformId == request.PlaftformId)
                .OrderBy(o => o.Id)
                .ProjectToType<WellDto>(_mapper.Config)
                .PaginatedListAsync(request.PageNumber, request.PageSize);

            return list.Items.Any() ? ServiceResult.Success(list) : ServiceResult.Failed<PaginatedList<WellDto>>(ServiceError.NotFound);
        }
    }
}
