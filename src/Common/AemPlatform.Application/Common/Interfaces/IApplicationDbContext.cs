﻿using AemPlatform.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading;
using System.Threading.Tasks;

namespace AemPlatform.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        public DbSet<AemPlatform.Domain.Entities.Platform> Platform { get; set; }
        public DbSet<Well> Well { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
      
    }
}
