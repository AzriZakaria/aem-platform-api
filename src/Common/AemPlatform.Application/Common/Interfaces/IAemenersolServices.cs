﻿using AemPlatform.Application.Common.Models;
using AemPlatform.Application.ExternalServices.Aemenersol.Request;
using AemPlatform.Application.ExternalServices.Aemenersol.Response;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AemPlatform.Application.Common.Interfaces
{
    public interface IAemenersolServices
    {
        Task<ServiceResult<List<PlatformWellActualResponse>>> GetPlatformDataActual(string token,CancellationToken cancellationToken);

        Task<ServiceResult<List<PlatformWellDummyResponse>>> GetPlatformDummy(string token,CancellationToken cancellationToken);

        Task<ServiceResult<string>> GetToken(LoginRequest request,CancellationToken cancellationToken);
    }
}
