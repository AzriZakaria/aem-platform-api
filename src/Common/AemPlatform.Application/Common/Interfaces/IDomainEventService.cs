﻿using System.Threading.Tasks;
using AemPlatform.Domain.Common;

namespace AemPlatform.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
