﻿using AemPlatform.Application.Dto;

namespace AemPlatform.Application.ApplicationUser.Queries.GetToken
{
    public class LoginResponse
    {
        public ApplicationUserDto User { get; set; }

        public string Token { get; set; }
    }

    public class TokenResponse
    {
      

        public string Token { get; set; }
    }
}
