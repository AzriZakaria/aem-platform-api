﻿using AemPlatform.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AemPlatform.Infrastructure.Persistence.Configurations
{
    public class WellConfigurations : IEntityTypeConfiguration<Well>
    {
        public void Configure(EntityTypeBuilder<Well> config)
        {
            config.HasKey(x => x.Id);

            config.Property(o => o.Id).ValueGeneratedNever();

            config.Property(o => o.UniqueName).HasMaxLength(100);
            config.HasIndex(o => o.UniqueName).IsUnique();

            config.Property(o => o.Longitude).HasPrecision(18, 9);
            config.Property(o => o.Latitude).HasPrecision(18, 9);



        } 
    }
}
