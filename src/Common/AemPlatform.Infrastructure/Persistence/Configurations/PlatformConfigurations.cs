﻿using AemPlatform.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AemPlatform.Infrastructure.Persistence.Configurations
{
    public class PlatformConfigurations : IEntityTypeConfiguration<Platform>
    {
        public void Configure(EntityTypeBuilder<Platform> config)
        {
            config.HasKey(x => x.Id);

            config.Property(o => o.Id).ValueGeneratedNever();

            config.Property(o => o.UniqueName).HasMaxLength(100);
            config.HasIndex(o => o.UniqueName).IsUnique();


            config.Property(o => o.Longitude).HasPrecision(18, 9);
            config.Property(o => o.Latitude).HasPrecision(18, 9);

            config.HasMany(o => o.Wells)
              .WithOne(c => c.Platform)
              .HasForeignKey(c => c.PlatformId)
              .OnDelete(DeleteBehavior.Restrict);

        } 
    }
}
