﻿using Microsoft.AspNetCore.Identity;
using AemPlatform.Domain.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace AemPlatform.Infrastructure.Persistence
{
    public static class ApplicationDbContextSeed
    {
        public static async Task SeedDefaultUserAsync(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            var administratorRole = new IdentityRole("Administrator");

            if (roleManager.Roles.All(r => r.Name != administratorRole.Name))
            {
                await roleManager.CreateAsync(administratorRole);
            }

            var defaultUser = new ApplicationUser { UserName = "user@aemenersol.com", Email = "user@aemenersol.com" };

            if (userManager.Users.All(u => u.UserName != defaultUser.UserName))
            {
                await userManager.CreateAsync(defaultUser, "Test@123");
                await userManager.AddToRolesAsync(defaultUser, new[] { administratorRole.Name });
            }
        }

    }
}
