﻿using System;
using AemPlatform.Application.Common.Interfaces;

namespace AemPlatform.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now.ToUniversalTime();
    }
}
