﻿using AemPlatform.Application.Common.Interfaces;
using AemPlatform.Application.Common.Models;
using AemPlatform.Application.ExternalServices.Aemenersol.Request;
using AemPlatform.Application.ExternalServices.Aemenersol.Response;
using AemPlatform.Domain.Enums;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AemPlatform.Infrastructure.Services
{
    public class AemenersolServices : IAemenersolServices
    {
        private string ClientApi { get; } = "aemenersol-api";
        private readonly IHttpClientHandler _httpClient;

        public AemenersolServices(IHttpClientHandler httpClient)
        {
           
            _httpClient = httpClient;
        }

        public async Task<ServiceResult<List<PlatformWellActualResponse>>> GetPlatformDataActual(string token,CancellationToken cancellationToken)
        {

            return await _httpClient.GenericRequestWithAuthorization<object, List<PlatformWellActualResponse>>(ClientApi
                        , "api/PlatformWell/GetPlatformWellActual"
                        ,  token
                        , cancellationToken
                        , MethodType.Get);
        }

        public async Task<ServiceResult<List<PlatformWellDummyResponse>>> GetPlatformDummy(string token, CancellationToken cancellationToken)
        {
            return await _httpClient.GenericRequestWithAuthorization<object, List<PlatformWellDummyResponse>> (ClientApi
                      , "api/PlatformWell/GetPlatformWellDummy"
                      , token
                      , cancellationToken
                      , MethodType.Get);
        }

        public async Task<ServiceResult<string>> GetToken(LoginRequest request, CancellationToken cancellationToken)
        {
            return await _httpClient.GenericRequest<LoginRequest, string>(ClientApi
                         , "/api/Account/Login"
                         , cancellationToken
                         , MethodType.Post,request);

        }
    }
}
