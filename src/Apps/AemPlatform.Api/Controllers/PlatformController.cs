﻿using AemPlatform.Application.Common.Models;
using AemPlatform.Application.Dto;
using AemPlatform.Application.Platform.Commands.Create;
using AemPlatform.Application.Platform.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace AemPlatform.Api.Controllers
{
    [Authorize]
    [Route("api/platform")]   
    public class PlatformController : BaseApiController
    {
        [HttpGet]
        public async Task<ActionResult<ServiceResult<PaginatedList<PlatformDto>>>> GetAllPlatformWithPagination([FromQuery]GetPlatformWithPaginationQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet]
        [Route("{id}/wells")]
        public async Task<ActionResult<ServiceResult<PaginatedList<WellDto>>>> GetWell([FromRoute] int id,[FromQuery]int pageNumber = 1, [FromQuery] int pageSize = 10)
        {
            var query = new GetWellsWithPaginationQuery { PlaftformId = id, PageNumber = pageNumber, PageSize = pageSize };

            return Ok(await Mediator.Send(query));
        }


        [HttpPost]
        [ProducesResponseType(typeof(ServiceResult<bool>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ServiceResult<bool>>> Create([FromBody]CreatePlatformDataFromApiCommand command)
        {
            return Ok(await Mediator.Send(command));
        }



    }
}
