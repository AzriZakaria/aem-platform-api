﻿using System.Threading.Tasks;
using AemPlatform.Application.ApplicationUser.Queries.GetToken;
using AemPlatform.Application.Common.Models;
using Microsoft.AspNetCore.Mvc;

namespace AemPlatform.Api.Controllers
{
    public class LoginController : BaseApiController
    {
        [HttpPost]
        public async Task<ActionResult<ServiceResult<LoginResponse>>> Create(GetTokenQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
    }
}
