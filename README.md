# Aem Platform Api

This is technical assessment to query the data from AEM Eenersol api following the principles of Clean Architecture.

## Technologies & Tools

- ASP.NET Core 5
- EF Core 5
- MediatR
- Mapster
- FluentValidation
- Logging
- Swagger
- JWT Authentication
- SQL Server local DB
- ASP.NET Identity

## Getting Started
Pull the source code, restore using the nuget then build and run. Since this using the first code approach so the database will created automatic. To view list of this `apis`, please access the swagger at `https://{your-domain}/swagger/index.html`

## Generate the JWT Token
You can use swagger to perform this action. Set the username and password and it will return JWT Token. Please use JWT token on `Authorization` attribute in `http header`

The username and password is the `same credentials` with AEM Enersol API

## Pull the data from AEM Eenersol
To pull the data, use `https://localhost:5021/api/platform` endpoint (post method) to get the data. Please select which dataset you want pull. Either actual or dummy data.

Data will store in the `localdb`

## View the data
Use `https://localhost:5021/api/platform` endpoint (get method) to view the data from `localdb`. Is able to do pagination so you can set which page and row data.

You also able to view `well` data on the selected `platform` by accessing this `https://localhost:5021/api/platform/{platformid}/well`

For your information, this two endpoint need to authorize first.

## Techincal Assessment (SQL QUERY)

Here the SQL Query to get last updated well for each platform

```sql
SELECT P.UniqueName as PlatformName, W.Id, P.Id as PlatformId, W.UniqueName, W.Latitude,W.Longitude,W.CreatedAt,W.UpdatedAt
FROM Platform P INNER JOIN (SELECT PlatformId, MAX(UpdatedAt) as MaxTime FROM Well GROUP BY PlatformId) LatestUpdate
on LatestUpdate.PlatformId = P.Id
INNER JOIN Well W on LatestUpdate.PlatformId = W.PlatformId AND LatestUpdate.MaxTime = W.UpdatedAt
ORDER BY P.UniqueName;
```

Please let me know if you have any further question














